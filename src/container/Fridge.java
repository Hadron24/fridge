package container;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import aliment.*;
import app.*;

public class Fridge {
	
	private List<Aliment> aliments;
	private int capacity = 10000;
	private int fullness = 0;
	private Application app = new Application();
	public Fridge() {
		aliments = new ArrayList<Aliment>();
	}
	
	public boolean add(Aliment aliment) throws AlimentException {
		
		int size = 0;
		
		if (aliment.getClass().equals(Food.class)) {
			size = ((Food) aliment).getAmountInGram();
		} else if (aliment.getClass().equals(Drink.class)) {
			size = ((Drink) aliment).getAmountInLiter();
		}

		if (checkCapacity(size)) {
			fullness += size;
			aliments.add(aliment);
			Logger.log(aliment, "added");
			return true;
		} else {
			throw new AlimentException("Food size is too large - doesn't fit into fridge!");
		}
	}
	
	public void remove(String ID) {
		for (final Iterator<Aliment> iter = aliments.iterator(); iter.hasNext();) {
			Aliment a = iter.next();
			if (a.getID().equals(ID)) {
				iter.remove();
				Logger.log(a, "removed");
			}
		}
	}
	
	public void edit(String ID)
	{
		Scanner scan = new Scanner(System.in);
		Date eDate = null;
		Aliment temp = findAlimentByID(ID);
		remove(ID);
		
		System.out.println("Name of the aliment: ");
		String name = scan.next();
		temp.setName(name);
		
		System.out.println("How much calories does the aliment have: ");
		int calories = scan.nextInt();
		temp.setCalories(calories);
		
		System.out.println("4. When this aliment created ? (type <Dec 25, 1997 10:10:10> format): ");
		String date = scan.next();
		try {
			DateFormat format = Application.getDateFormat();
			eDate = format.parse(date);
		} catch (ParseException e) {
			System.err.println("Unable to parse this date. Using current date.\n");
			eDate = new Date();
		}
		temp.setExpirationDate(eDate);
		
		try {
			this.add(temp);
		} catch (AlimentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Aliment> queryAliments() {
		return null;
	}
	
	public ArrayList<String> checkDates() {
		//Calendar calendar = Calendar.getInstance();
		Date today = new Date (); 
		SimpleDateFormat sdf = app.getDateFormat();
		String today_string = sdf.format(today);
		
		ArrayList<String> expiredIDs = new ArrayList<String>();
		
		for (Aliment aliment : aliments) {
			Date expirationDate = aliment.getExpirationDate();
			//if (expirationDate.after(calendar.getTime()) || expirationDate.equals(calendar.getTime())) 
			//if(expirationDate.equals(today_string))
			if(expirationDate.after(today) || expirationDate.equals(today))
			{
				expiredIDs.add(aliment.getID());
			}
		}
		
		return expiredIDs;
	}
	
	public boolean checkCapacity(int size) {
		if (fullness + size <= capacity) 
			return true;
		else 
			return false;
	}
	
	public Aliment findAlimentByID(String ID) {
		for (Aliment a : aliments) {
			if (a.getID().equals(ID)) {
				return a;
			}
		}
		return null;
	}
	
	
	// Getters and Setters
	public List<Aliment> getAliments() {
		return aliments;
	}

	public void setAliments(List<Aliment> aliments) {
		this.aliments = aliments;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getFullness() {
		return fullness;
	}

	public void setFullness(int fullness) {
		this.fullness = fullness;
	}
	
}