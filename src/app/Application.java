package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Scanner;

import aliment.Aliment;
import aliment.AlimentException;
import aliment.Drink;
import container.Fridge;
import aliment.Food;

public class Application {
	
	// ID: Admin
	// Password: 1
	
	private static Logger logger;
	private static PersonAdapter personAdapter;
	private static Fridge fridge;
	private static Application application;
	static long time = System.currentTimeMillis();
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
	static List<String> ingre = new ArrayList<String>();
	Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args){
		application = new Application();
		application.run();
	}
	
	public void run()  {
		
		// Initialize personlist
		personAdapter = new PersonAdapter();
		
		// Use-case 2 : add food to fridge 
		fridge = new Fridge();
		fridge.setCapacity(50000); // 50 kg of food or 50 l of drink
		
		// Use-case 3: periodically check dates in the fridge
		Timer timer = new Timer();
		timer.schedule(new CheckDateTimer(), 60000); // check dates every minute (for now)
		
		Date today = new Date (); 
		String test = sdf.format(today);
		System.out.println ("Current time: " + test);
		//login 
		check_family();
		
		//selection of work
		while(true)
		{
			System.out.print("1. Add 2. Remove 3. Edit 4. Print aliments 5. exit : ");
			int selection = scan.nextInt();
			switch(selection)
			{
				case 1 :
					System.out.println("1. Choose things to add in fridge");
					System.out.print("\t1. Food 2. Drink : ");
					int selection2 = scan.nextInt();
					
					switch(selection2)
					{
					case 1: 
						add_food();
						break;
					case 2: 
						add_drink();
						break;
					}
					break;
				case 2 : 
					remove();
					break;
				case 3 :
					edit();
					break;
				case 4:
					printAliments();
					break;
				case 5:
					System.out.println("Program terminates");
					System.exit(0);
			}
		}
		
	}
	
	public void check_family()
	{
		while(true)
		{	
			System.out.println("┌───────────────────────────────────────────────────────────────────────┐");
	         System.out.println("│                     Enter ID, Password                                │");
	         System.out.println("└───────────────────────────────────────────────────────────────────────┘");
	         
	         System.out.print("ID : ");
	         String ID = scan.next();
	         System.out.print("Password : ");
	         String Password = scan.next();
	         
	         if(login(ID, Password) == true)
	         {
	        	Person p = personAdapter.getPersonByID(ID);
	        	String Name = "";
	        	if (p != null) {
	        		Name = p.getName();
	        	}
	            System.out.println("┌───────────────────────────────────────────────────────────────────────┐");
	            System.out.printf("│                    Login successfully ! Welcome  %10s           │\n", Name);
	            System.out.println("└───────────────────────────────────────────────────────────────────────┘");
	         }
	         else
	         {
	            System.out.println("┌───────────────────────────────────────────────────────────────────────┐");
	            System.out.println("│           Login failed, please check ID, password and name            │");
	            System.out.println("└───────────────────────────────────────────────────────────────────────┘");
	            System.out.print("Retry? (y/n) : ");
	            
	            String y_or_n = scan.next();
	            if(y_or_n.equals("y"))
	            {
	               continue;
	            }
	            if(y_or_n.equals("n"))
	            {
	               System.out.println("┌───────────────────────────────────────────────────────────────────────┐");
	               System.out.println("│                             Program terminates                        │");
	               System.out.println("└───────────────────────────────────────────────────────────────────────┘");
					System.exit(0);
				}
			}
			
			break;
		}
	}
	
	// Use-case 1: try to login
	public boolean login(String ID, String password) {
		
		boolean loginResult = false;
		
		for (Person person : personAdapter.getPersonList()) {
			if (person.getID().equals(ID)) {
				if (person.getPassword().equals(password)) {
						loginResult = true;
				}
			}
		}
		
		return loginResult;
	}
	
	public void add_food(){
		// Try to add aliment to fridge

		Date conFromDate = new Date();
		//Calendar cal = Calendar.getInstance();
		
		//food name 
		System.out.print("2. What is name of food ? : ");
		String food_name = scan.next();
		
		//define vegetable
		System.out.print("3. Is food vegetable? (y/n): ");
		String bool_type = scan.next();
		boolean bool_type_food = false;
		if( bool_type.equals("y") )
			bool_type_food = true;
		if( bool_type.equals("n") )
			bool_type_food = false;
		
		//Created date
		System.out.print("4. When this food created ? (type <1997.04.02. 12:13:14> format) : ");
		scan.nextLine();//refresh buffer
		String test = scan.nextLine();
		Date creationDate = new Date();
		try {
			creationDate = sdf.parse(test);
			String formatted_date = sdf.format ( creationDate );
			System.out.println(formatted_date);
		} catch (ParseException e1) {
			System.err.println("Unable to parse input date! Using current date.\n");
		}

		//calculate expire date
		System.out.print("5. Day to the expire : ");
		int expire_date = scan.nextInt();
		Calendar cCal = Calendar.getInstance();
		cCal.setTime(new Date());
		cCal.add(Calendar.DAY_OF_YEAR, expire_date);
		String expireDate = sdf.format(cCal.getTime());
		System.out.println(expireDate);
		
		System.out.print("6. Calories : ");
		int calories = scan.nextInt();
		
		//Gram
		System.out.print("7. The gram of food : ");
		int gram = scan.nextInt();
		
		try {
			Food f = new Food(food_name, sdf.parse(expireDate));
			//f.setID(adder);
			f.setName(food_name);
			ingre.add(food_name);
			f.setCreationDate(creationDate);
			f.setCalories(calories);
			f.setAmountInGram(gram);
			f.setVegetarian(bool_type_food);
			f.setIngredients(ingre);
			f.setType("Food");
			fridge.add(f);
		} catch(AlimentException e) {
			System.err.println("Error with adding to aliment.\n");
		} catch(ParseException e) {
			System.err.println("Error with parsing the given date!\n");
		} catch(Exception e) {
			System.err.println("Error! Cannot add to fridge.\n");
		}
		
			
	}
	
	public void add_drink()
	{
		Date conFromDate = new Date();
		
		//food name 
		System.out.print("2. What is name of drink ? : ");
		String drink_name = scan.next();
		//calculate expire date
		
		System.out.print("3. Is drink fizzy? (y/n): ");
		String bool_type = scan.next();
		boolean bool_type_drink = false;
		if( bool_type.equals("y") )
			bool_type_drink = true;
		if( bool_type.equals("n") )
			bool_type_drink = false;
		
		System.out.print("4. When this drink created ? (type <1997.04.02. 12:13:14> format) : ");
		scan.nextLine();//refresh buffer
		String test = scan.nextLine();
		Date creationDate = new Date();
		try {
			creationDate = sdf.parse(test);
			String formatted_date = sdf.format ( creationDate );
			System.out.println(formatted_date);
		} catch (ParseException e1) {
			System.err.println("Unable to parse input date! Using current date.\n");
		}
		
		System.out.print("5. Day to the expire : ");
		int expire_date = scan.nextInt();
		Calendar cCal = Calendar.getInstance();
		cCal.setTime(new Date());
		cCal.add(Calendar.DAY_OF_YEAR, expire_date);
		String expireDate = sdf.format(cCal.getTime());
		
		System.out.print("6. Calories : ");
		int calories = scan.nextInt();
		
		//Gram
		System.out.print("7. Liters of drink : ");
		int liter = scan.nextInt();
		
		try {
			Drink d = new Drink(drink_name, sdf.parse(expireDate));
			d.setName(drink_name);
			ingre.add(drink_name);
			d.setCreationDate(creationDate);
			d.setCalories(calories);
			d.setAmountInLiter(liter);
			d.setFizzy(bool_type_drink);
			d.setIngredients(ingre);
			d.setType("Drink");
			fridge.add(d);
		} catch(AlimentException e) {
			System.err.println("Error with adding to aliment.\n");
		} catch(ParseException e) {
			System.err.println("Error with parsing the given date!\n");
		} catch(Exception e) {
			System.err.println("Error! Cannot add to fridge.\n");
		}
	}
	
	public void edit(){
		printAliments();
		System.out.print("1. Which Aliment will be edited ? Type number : ");
		int edit_number = scan.nextInt();
		edit_number -= 1;
		Date conFromDate = new Date();
		
		try {
			Aliment a = fridge.getAliments().get(edit_number);
			fridge.edit(a.getID());
		} catch(IndexOutOfBoundsException e) {
			System.err.println("Bad index.\n");
		}
	}
	
	public void remove(){
		
		printAliments();
		
		System.out.print("1. Which Aliment will be removed ? Type number : ");
		int remove_number = scan.nextInt();
		remove_number -= 1;
		
		try {
			Aliment a = fridge.getAliments().get(remove_number);
			fridge.remove(a.getID());
		} catch(IndexOutOfBoundsException e) {
			System.err.println("Bad index.\n");
		}
	}
	
	public void printAliments() {
		for (int i = 0 ; i < fridge.getAliments().size() ; i++){
			Aliment a = fridge.getAliments().get(i);
			System.out.println(i+1 +". name: " + a.getName() + "\n\tType: " + a.getType() + 
							   "\n\tCalories: " + a.getCalories() + "\n\tCreation date: " + sdf.format(a.getCreationDate()));
		}
			
	}
	
	
	public List<Aliment> query() {
		return fridge.getAliments();
	}
	
	
	class CheckDateTimer extends TimerTask {
	      public void run() {
	         ArrayList<String> expiredIDs = fridge.checkDates();
	         
	         //get current time
	         long t = System.currentTimeMillis();
	         Date now = new Date(t);
	         
	         // Print out IDs of expired food/drink 
	         for (String ID : expiredIDs) {
	            Aliment a = fridge.findAlimentByID(ID);
	            String name = a.getName();
	            
	            //compare between an expiration date of an aliment and now
	            if(now.after(a.getExpirationDate()))
	               Logger.log(a, "expired");
	         }
	      }
	   }
	
	public static SimpleDateFormat getDateFormat() {
		return sdf;
	}
}


