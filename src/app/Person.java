package app;

public class Person {
	
	private String name;
	private String password;
	private String ID;
	
	public Person(String name, String password, String ID){
		this.name = name;
		this.password = password;
		this.ID = ID;
	}
	
	// Getters and Setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	
}
