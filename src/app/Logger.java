package app;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import aliment.Aliment;
import aliment.Drink;
import aliment.Food;

public class Logger {
	
	public static void log(Aliment aliment, String message) {
		
		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", true)))) {
			
			String type = "";
			
			if (aliment.getClass().equals(Drink.class)) {
				type = "D";
			} else if (aliment.getClass().equals(Food.class)) {
				type = "F";
			}
			
			String log = type + " " + aliment.getName() + " (ID: " + aliment.getID() + ")" + ": " + message + ".";
			
			out.println(log);
			System.out.println("Logger: " + log);
			
		} catch (IOException e) {
			System.err.println("Exception: I/O");
		}
		
	}
}
