package app;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PersonAdapter {
	
	private List<Person> personList;
	
	
	public PersonAdapter() {
		personList = new ArrayList<Person>();
		readFile();
	}
	
	public void readFile() {
		try {
			Scanner sc = new Scanner(new FileReader("members.txt"));
			while (sc.hasNextLine()) {
				
				String ID = sc.next();
				String password = sc.next();
				String name = sc.next();
				
				Person p = new Person(name, password, ID);
				personList.add(p);
				
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.err.println("File not found: members.txt");
		}
	}
	
	public Person getPersonByID(String ID) {
		for (Person p : personList) {
			if (p.getID().equals(ID)) {
				return p;
			}
		}
		return null;
	}
	
	// Getters and Setters
	public List<Person> getPersonList() {
		return personList;
	}

	public void setPersonList(List<Person> personList) {
		this.personList = personList;
	}
	
}
