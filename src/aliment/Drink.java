package aliment;

import java.util.Date;

public class Drink extends Aliment {
	
	public Drink(String name, Date expirationDate) {
		super(name, expirationDate);
	}
	private boolean isFizzy;
	private int amountInLiter;
	
	// Getters and Setters
	public boolean isFizzy() {
		return isFizzy;
	}
	public void setFizzy(boolean isFizzy) {
		this.isFizzy = isFizzy;
	}
	public int getAmountInLiter() {
		return amountInLiter;
	}
	public void setAmountInLiter(int amountInLiter) {
		this.amountInLiter = amountInLiter;
	}
	
}
