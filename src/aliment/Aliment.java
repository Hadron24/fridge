package aliment;

import java.util.Date;
import java.util.List;

import app.Application;

public class Aliment {
	
	private String name;
	private Date creationDate;
	private Date expirationDate;
	private int calories;
	private List<String> ingredients;
	private String ID;
	private String type;
	
	private static int IDcounter = 0;
	
	public Aliment(String name, Date expirationDate) {
		
		String date = Application.getDateFormat().format(new Date());
		
		this.ID = name + date;
		this.name = name;
		this.expirationDate = expirationDate;
	}
	
	// Getters and Setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public int getCalories() {
		return calories;
	}
	public void setCalories(int calories) {
		this.calories = calories;
	}
	public List<String> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<String> ingredients) {
		this.ingredients = ingredients;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	
	public String getType(){
		return type;
	}
	
	public void setType(String type){
		this.type = type;
	}
}
