package aliment;

import java.util.Date;

public class Food extends Aliment{
	
	public Food(String name, Date expirationDate) {
		super(name, expirationDate);
	}
	private boolean isVegetarian;
	private int amountInGram;
	
	// Getters and Setters
	public boolean isVegetarian() {
		return isVegetarian;
	}
	public void setVegetarian(boolean isVegetarian) {
		this.isVegetarian = isVegetarian;
	}
	public int getAmountInGram() {
		return amountInGram;
	}
	public void setAmountInGram(int amountInGram) {
		this.amountInGram = amountInGram;
	}
	
}
